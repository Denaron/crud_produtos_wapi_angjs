﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Projeto.Services.Models; //camada de modelo..
using Projeto.Entities; //entidades..
using Projeto.Repository.Persistence; //acesso a dados..

namespace Projeto.Services.Controllers
{
    [RoutePrefix("api/produto")] //URL base..
    public class ProdutoController : ApiController
    {
        [HttpPost]
        [Route("cadastrar")] //URL: /api/produto/cadastrar
        public HttpResponseMessage Cadastrar(ProdutoModelCadastro model)
        {
            try
            {
                Produto p = new Produto(); //classe de entidade..
                p.Nome = model.Nome;
                p.Preco = model.Preco;
                p.Quantidade = model.Quantidade;
                p.DataCompra = model.DataCompra;

                //gravar no banco de dados..
                ProdutoRepository rep = new ProdutoRepository();
                rep.Insert(p); //cadastrando..

                //retornar status de sucesso
                return Request.CreateResponse(HttpStatusCode.OK, "Produto cadastrado com sucesso.");
            }
            catch(Exception e)
            {
                //retornar status de erro
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }


        [HttpPut]
        [Route("atualizar")] //URL: /api/produto/atualizar
        public HttpResponseMessage Atualizar(ProdutoModelEdicao model)
        {
            try
            {
                Produto p = new Produto(); //classe de entidade..
                p.IdProduto = model.IdProduto;
                p.Nome = model.Nome;
                p.Preco = model.Preco;
                p.Quantidade = model.Quantidade;
                p.DataCompra = model.DataCompra;

                //atualizar no banco de dados..
                ProdutoRepository rep = new ProdutoRepository();
                rep.Update(p); //atualizando..

                //retornar status de sucesso
                return Request.CreateResponse(HttpStatusCode.OK, "Produto atualizado com sucesso.");
            }
            catch (Exception e)
            {
                //retornar status de erro
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpDelete]
        [Route("excluir")] //URL: /api/produto/excluir?id={0}
        public HttpResponseMessage Excluir(int id)
        {
            try
            {
                ProdutoRepository rep = new ProdutoRepository();
                Produto p = rep.FindById(id); //buscando no banco de dados..

                if(p != null) //se o produto foi encontrado..
                {
                    rep.Delete(p); //excluindo..
                    //retornar status de sucesso
                    return Request.CreateResponse(HttpStatusCode.OK, "Produto excluido com sucesso.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Produto não encontrado.");
                }                
            }
            catch (Exception e)
            {
                //retornar status de erro
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("listar")] //URL: /api/produto/listar
        public HttpResponseMessage Listar()
        {
            try
            {
                List<ProdutoModelConsulta> lista = new List<ProdutoModelConsulta>();

                ProdutoRepository rep = new ProdutoRepository();
                foreach(Produto p in rep.FindAll())
                {
                    ProdutoModelConsulta model = new ProdutoModelConsulta();
                    model.IdProduto = p.IdProduto;
                    model.Nome = p.Nome;
                    model.Preco = p.Preco;
                    model.Quantidade = p.Quantidade;
                    model.Total = p.Preco * p.Quantidade;
                    model.DataCompra = p.DataCompra;

                    lista.Add(model); //adicionar na lista..
                }

                //retornar status de sucesso
                return Request.CreateResponse(HttpStatusCode.OK, lista);
            }
            catch (Exception e)
            {
                //retornar status de erro
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("obter")] //URL: /api/produto/obter?id={0}
        public HttpResponseMessage Obter(int id)
        {
            try
            {
                ProdutoRepository rep = new ProdutoRepository();
                Produto p = rep.FindById(id); //buscando pelo id..

                if(p != null) //se o produto foi encontrado..
                {
                    ProdutoModelConsulta model = new ProdutoModelConsulta();

                    model.IdProduto = p.IdProduto;
                    model.Nome = p.Nome;
                    model.Preco = p.Preco;
                    model.Quantidade = p.Quantidade;
                    model.Total = p.Preco * p.Quantidade;
                    model.DataCompra = p.DataCompra;

                    //retornar status de sucesso
                    return Request.CreateResponse(HttpStatusCode.OK, model);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Produto não encontrado.");
                }
            }
            catch (Exception e)
            {
                //retornar status de erro
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
