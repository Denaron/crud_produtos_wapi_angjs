﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration; //connectionstring..
using System.Data.Entity; //entityframework..
using System.Data.Entity.ModelConfiguration.Conventions;
using Projeto.Entities; //entidades
using Projeto.Repository.Mappings; //mapeamento..

namespace Projeto.Repository.Connections
{
    public class Conexao : DbContext
    {
        public Conexao()
            : base(ConfigurationManager.ConnectionStrings["aula"].ConnectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //configuração..
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new ProdutoMap());
        }

        public DbSet<Produto> Produto { get; set; }
    }
}
