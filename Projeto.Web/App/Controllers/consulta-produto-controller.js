﻿//declarando um controller AngularJS

/*
    $scope -> comunicar o conteudo da página com o controller
    $http  -> comunicar o controller com os serviços da API
*/

app.controller('produtoConsultaController',
    function ($scope, $http) {

        //criando uma função para executar o serviço de consulta da API
        $scope.consultar = function () {

            //executar o serviço de consulta..
            $http.get(urlBase + "api/produto/listar")
                .success(function (dados) {

                    //enviando o retorno do serviço para a página..
                    $scope.listagemProdutos = dados;

                })
                .error(function (e) {
                    $scope.mensagem = e;
                });
        };
    

$scope.obter = function (id) {

    $http.get(urlBase + "api/produto/obter?id=" + id)
        .success(function (dados) {
        
            $scope.model = dados;

    })
        .error(function (e) {
        $scope.mensagem = e;
        });
    };

$scope.excluir = function () {

    $http.delete(urlBase + "api/produto/excluir?id=" + $scope.model.IdProduto).success(function (msg){
        
        $scope.mensagem = msg;

        $scope.consultar();

    }).error(function (e){
        $scope.mensagem = e;
    });

}

$scope.atualizar = function () {

    $http.put(urlBase + "api/produto/atualizar", $scope.model).success(function msg() {
        $scope.mensagem = msg;
        $scope.consultar();
    })
    .error(function(e) {
        $scope.mensagem = e;
    })
    };

});

