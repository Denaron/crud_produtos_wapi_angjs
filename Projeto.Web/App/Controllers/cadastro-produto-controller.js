﻿//declarando um controller AngularJS

/*
    $scope -> comunicar o conteudo da página com o controller
    $http  -> comunicar o controller com os serviços da API
*/

app.controller('produtoCadastroController',
    function ($scope, $http) {

        //criando a função de cadastro do produto..
        $scope.cadastrar = function () {

            //exibir mensagem..
            $scope.mensagem = "Enviando dados, por favor aguarde...";

            //executar o serviço no webapi..
            $http.post(urlBase + "api/produto/cadastrar", $scope.model)
                .success(function (msg) { //requisição bem-sucedida
                    $scope.mensagem = msg; //mensagem do serviço..
                    $scope.model = {}; //vazio..
                })
                .error(function (e) { //requisição falhou
                    //imprimir o erro..
                    $scope.mensagem = e;
                });
        }

    });