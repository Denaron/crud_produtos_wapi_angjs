﻿//criar o modulo em angular..
//ngRoute -> biblioteca do angular para mapeamento de páginas em SPA
var app = angular.module('projeto', ['ngRoute']);

//variavel global..
var urlBase = "http://localhost:50473/";

//mapear as rotas do projeto..
//$routeProvider -> objeto para mapeamento de rotas (ngRoute)
app.config(function ($routeProvider) {

    //criando um mapeamento de rotas..
    $routeProvider
        .when(
            '/produto/cadastro', //URL
            {
                templateUrl: '/App/Views/cadastro-produto.html',
                controller: 'produtoCadastroController'
            }
        )
        .when(
            '/produto/consulta', //URL
            {
                templateUrl: '/App/Views/consulta-produto.html',
                controller: 'produtoConsultaController'
            }
        );
});